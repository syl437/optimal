import { Component,NgZone } from '@angular/core';
import {SocialSharing} from "@ionic-native/social-sharing";
import {LaunchNavigator} from "@ionic-native/launch-navigator";
import {WazeModalComponent} from "../waze-modal/waze-modal";
import {ModalController, NavController} from "ionic-angular";
import {sent_to_server_service} from "../../services/sent_to_server_service";
import {Config} from "../../services/config";
import {CompanyPage} from "../../pages/company/company";


/**
 * Generated class for the FooterComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'footer',
  templateUrl: 'footer.html'
})
export class FooterComponent {
    
    //public Message = "סובל/ת מהשמנה מיואש מדיאטות ורוצה ירידה מהירה במשקל ללא ניתוח צפי בבלון ההרזיה החדשני שכובש את העולם"
    //public Image = "http://www.tapper.co.il/baloon/logo.png"
    //public AppUrl = "https://www.hadassahop.org.il/"
    //public PhoneNumber = "+972524641074";
    public CustomerId;
    public host;
    CustumerServerData:any = [];
    customers_branches:any = [];
    public imageLogo:any;

    public Message;
    public Image;
    public AppUrl;
    public PhoneNumber;
    public Email;
    public MailSubject;

    public AppTitle:any;
    public AppWebSite:any;
    public AppDesc:any;


  constructor(public navCtrl: NavController,private socialSharing: SocialSharing , private launchNavigator: LaunchNavigator , public modalCtrl: ModalController,public sendtoserver:sent_to_server_service,public zone: NgZone, public Settings:Config) {
      this.CustomerId = this.Settings.CustomerId;
      this.host = this.Settings.host;
      this.sendtoserver._JsonInfo.subscribe(val => {
          this.zone.run(() => {
              this.CustumerServerData = val;
              console.log("CustumerServerData",this.CustumerServerData)
              if (this.CustumerServerData) {
                  if (this.CustumerServerData.custumer_data) {

                      if (this.CustumerServerData.app_settings.length > 0) {
                          this.AppTitle =  this.CustumerServerData.app_settings[0].title;
                          this.AppWebSite =  this.CustumerServerData.app_settings[0].website;
                          this.AppDesc =  this.CustumerServerData.app_settings[0].description;
                      }


                      if (this.CustumerServerData.custumer_data.length > 0) {
                          this.MailSubject = this.CustumerServerData.custumer_data[0].name;
                          this.Image = this.host + this.CustumerServerData.custumer_data[0].image;
                          this.Message = this.CustumerServerData.custumer_data[0].description;
                          this.AppUrl = this.CustumerServerData.custumer_data[0].website;
                          this.PhoneNumber = this.CustumerServerData.custumer_data[0].phone;
                          this.Email = this.CustumerServerData.custumer_data[0].email;
                      }
                  }
              }
          });
      });
  }
    
    
    shareViaPhone() {
        window.location.href = "tel:"+this.PhoneNumber;
    }
    
    shareViaMail() {
        this.socialSharing.shareViaEmail(this.Message,"הערה, תלונה או בעיה", [this.Email]).then(() => {
            // Success!
        }).catch(() => {
            // Error!
        });
    }

    openCompanyPage() {
        this.navCtrl.push(CompanyPage);

    }
    
    share()
    {
        this.socialSharing.share(this.Message ,this.MailSubject,this.Image , this.AppWebSite);
    }
    
    shareViaWaze() {
        let noteModal = this.modalCtrl.create(WazeModalComponent);
        noteModal.present();

        noteModal.onDidDismiss(data => {
            console.log(data)
            this.launchNavigator.navigate([data.location_lat, data.location_lng]);
        });

       // this.launchNavigator.navigate([32.097143, 34.821450]);
        //window.location.href = "waze://?ll=32.123286,34.808958"
        //<a href='waze://?ll=31.99090,34.77444'>Let's go to the mall</a>
    }

}
