
import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import {Config} from "./config";
import {HomePage} from "../pages/home/home";


@Injectable()

export class loginService
{
    public ServerUrl;
    public CustomerId;

    constructor(private http:Http,public Settings:Config) {
        this.ServerUrl = Settings.ServerUrl;
        this.CustomerId = Settings.CustomerId;
    };

    getUserDetails(url:string , info:Object)
    {
        //console.log("Info : " , info)
        let body = new FormData();
        body.append('customer_id',this.CustomerId);
        body.append('mail',info["mail"]);
        body.append('password',info["password"]);
        body.append('push',this.Settings.PushId);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{console.log("getUserDetails : " , data)}).toPromise();
    }

    facebookLogin(url:string , email:string)
    {
        //console.log("Info : " , info)
        let body = new FormData();
        body.append('customer_id',this.CustomerId);
        body.append('mail',email);
        body.append('push',this.Settings.PushId);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{console.log("getUserDetails : " , data)}).toPromise();
    }



    logOutUser()
    {
        window.localStorage.identify = '';
        this.Settings.UserId = 0;
    }



    ForgotPass(url:string , info:Object)
    {
        let body = new FormData();
        body.append('customer_id',this.CustomerId);
        body.append('info',JSON.stringify(info));
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{console.log("ForgotPass : " , data)}).toPromise();
    }

    RegisterUser(url:string , info:Object)
    {
        let body = new FormData();
        body.append('customer_id',this.CustomerId);
        body.append('info',JSON.stringify(info));
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{console.log("getUserDetails : " , data)}).toPromise();
    }
    
    sendRecommend(url:string , info:Object)
    {
        let body = new FormData();
        body.append('customer_id',this.CustomerId);
        body.append('info',JSON.stringify(info));
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{console.log("getUserDetails : " , data)}).toPromise();
    }
    
    getRecommendById(url:string)
    {
        let body = new FormData();
        body.append('customer_id',this.CustomerId);
        body.append('uid',window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{console.log("getUserDetails : " , data)}).toPromise();
    }
    
    getUserById(url:string)
    {
        let body = new FormData();
        body.append('customer_id',this.CustomerId);
        body.append('uid',window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{console.log("getUserDetails : " , data)}).toPromise();
    }
    
    SaveDetails(url:string , Details)
    {
        let body = new FormData();
        body.append('customer_id',this.CustomerId);
        body.append('uid',window.localStorage.id);
        body.append('selected',JSON.stringify(Details));
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{console.log("getUserDetails : " , data)}).toPromise();
    }

    requestWithdraw(url:string)
    {
        let body = new FormData();
        body.append('customer_id',this.CustomerId);
        body.append('uid',window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{console.log("getUserDetails : " , data)}).toPromise();
    }

    sendTestData(url:string , data:any)
    {
        let body = new FormData();
        body.append('customer_id',this.CustomerId);
        body.append('data',data);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{console.log("ForgotPass : " , data)}).toPromise();
    }

};


