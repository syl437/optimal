import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import {Config} from "./config";
import {HomePage} from "../pages/home/home";

import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import {Restangular} from 'ngx-restangular';
import {LoadingController} from "ionic-angular";

@Injectable()

export class sent_to_server_service
{
    public ServerUrl;
    public CustomerId;


    public _JsonInfo: BehaviorSubject<any> = new BehaviorSubject(null);
    JsonInfo$: Observable<any> = this._JsonInfo.asObservable();
    get JsonInfo(): Array<any> {return this._JsonInfo.getValue();}

    constructor(private http:Http,public Settings:Config,public restangular: Restangular,
                public loadingCtrl: LoadingController) {
        this.ServerUrl = Settings.ServerUrl;
        this.CustomerId = Settings.CustomerId;
    };

    async getCustumerServerData(url) {
        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();
        let body = new FormData();
        body.append('customer_id', this.CustomerId);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res).do((data) => {
            console.log("Notes : ", data.json());
            this._JsonInfo.next(data.json());
            loading.dismiss();
        }).toPromise();
    }


    addNote(url:string , info:Object)
    {
        let body = new FormData();
        body.append('customer_id',this.CustomerId);
        body.append('title',info["title"]);
        body.append('info',info["info"]);
        body.append('uid',window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res).do((data)=>{console.log("Notes : " , data)}).toPromise();
    }

    updateNote(url:string , info:Object , id)
    {
        let body = new FormData();
        body.append('customer_id',this.CustomerId);
        body.append('title',info["title"]);
        body.append('info',info["info"]);
        body.append('id',id);
        body.append('uid',window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res).do((data)=>{console.log("Notes : " , data)}).toPromise();
    }

    getAllNotes(url:string)
    {
        let body = new FormData();
        body.append('uid',window.localStorage.id);
        body.append('customer_id',this.CustomerId);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res).do((data)=>{console.log("Notes : " , data)}).toPromise();
    }

    deleteNote(url:string,id)
    {
        let body = new FormData();
        body.append('id',id);
        body.append('uid',window.localStorage.id);
        body.append('customer_id',this.CustomerId);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res).do((data)=>{console.log("Notes : " , data)}).toPromise();
    }


    //Bid

    addBid(url:string,job,info)
    {
        let body = new FormData();
        body.append('job',job);
        body.append('info',info);
        body.append('uid',window.localStorage.id);
        body.append('customer_id',this.CustomerId);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res).do((data)=>{console.log("Notes : " , data)}).toPromise();
    }


    getHomeDetails(url:string)
    {
        let body = new FormData();
        body.append('uid',window.localStorage.id);
        body.append('customer_id',this.CustomerId);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res).do((data)=>{console.log("Notes : " , data)}).toPromise();
    }

    updateJob(url:string , info:Object , id)
    {
        let body = new FormData();
        body.append('Date2',info['date']);
        body.append('Time2',info['time']);
        body.append('info',info['info']);
        body.append('id',id);
        body.append('uid',window.localStorage.id);
        body.append('customer_id',this.CustomerId);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res).do((data)=>{console.log("Notes : " , data)}).toPromise();
    }
};


