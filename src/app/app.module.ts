import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {ListPage} from '../pages/list/list';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {AboutPage} from "../pages/about/about";
import {FaqPage} from "../pages/faq/faq";
import {MyAccountPage} from "../pages/my-account/my-account";
import {SuggestPage} from "../pages/suggest/suggest";
import {ProfilePage} from "../pages/profile/profile";
import {LaunchNavigator} from "@ionic-native/launch-navigator";
import {SocialSharing} from "@ionic-native/social-sharing";
import {LoginPage} from "../pages/login/login";
import {loginService} from "../services/loginService";
import {Config} from "../services/config";
import {RegisterPage} from "../pages/register/register";
import {CompanyPage} from "../pages/company/company";
import {toastService} from "../services/toastService";
import {HttpModule} from "@angular/http";
import {FooterComponent} from "../components/footer/footer";
import {WazeModalComponent} from "../components/waze-modal/waze-modal";
import {sent_to_server_service} from "../services/sent_to_server_service";
import {RestangularModule} from 'ngx-restangular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Keyboard } from '@ionic-native/keyboard';
import { ForgotpassPage } from '../pages/forgotpass/forgotpass';
import { Facebook } from '@ionic-native/facebook';


export function RestangularConfigFactory(RestangularProvider) {

    // Setting up the Restangular endpoint - where to send queries
    RestangularProvider.setBaseUrl("http://tapper.org.il/baloon/laravel/public/api/");


    // Every time the query is made, it's console.logged.
    // Restangular doesn't support boolean answers from the API, so right now null is returned
    RestangularProvider.addResponseInterceptor((data, operation, what, url, response) => {

        if (data) {
            console.log(url, data, operation);
        }
        return data;
    });

    // Every time when error is received from the server API_ENDPOINT, first it is processed here.
    // IDM_ENDPOINT errors are processed in the functions only (login, refresh-tokens, get-users x 2).
    RestangularProvider.addErrorInterceptor(async (response, subject, responseHandler) => {

        console.log('ErrorInterceptor', response);

        // if (response.data && !response.success){
        //     if (response.data.error && response.data.error.message){
        //         alertCtrl.create({title: response.data.error.message, buttons: ['OK']}).present();
        //     } else {
        //         alertCtrl.create({title: 'Server error!', buttons: ['OK']}).present();
        //     }
        // }

    });

}


@NgModule({
    declarations: [
        MyApp,
        HomePage,
        ListPage,
        AboutPage,
        FaqPage,
        MyAccountPage,
        SuggestPage,
        ProfilePage,
        LoginPage,
        ForgotpassPage,
        RegisterPage,
        CompanyPage,
        FooterComponent,
        WazeModalComponent,

    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp),
        RestangularModule.forRoot([], RestangularConfigFactory),
        HttpModule,
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        ListPage,
        AboutPage,
        FaqPage,
        MyAccountPage,
        SuggestPage,
        ProfilePage,
        LoginPage,
        ForgotpassPage,
        RegisterPage,
        CompanyPage,
        FooterComponent,
        WazeModalComponent,

    ],
    providers: [
        StatusBar,
        SplashScreen,
        SocialSharing,
        loginService,
        toastService,
        Config,
        LaunchNavigator,
        sent_to_server_service,
        InAppBrowser,
        Keyboard,
        Facebook,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
    ]
})
export class AppModule {
}
