import { Component, ViewChild,enableProdMode } from '@angular/core';
import { Nav, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import {LoginPage} from "../pages/login/login";
import {loginService} from "../services/loginService";
import {sent_to_server_service} from "../services/sent_to_server_service";
import {Config} from "../services/config";
import {RegisterPage} from "../pages/register/register";
import { InAppBrowser } from '@ionic-native/in-app-browser';


import {AboutPage} from "../pages/about/about";
import {FaqPage} from "../pages/faq/faq";
import {MyAccountPage} from "../pages/my-account/my-account";
import {SuggestPage} from "../pages/suggest/suggest";
import {ProfilePage} from "../pages/profile/profile";


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  
  rootPage: any = "";//LoginPage;
    public browser;

    pages: Array<{ title: string, component: any , icon:string }>;

  constructor(public platform: Platform,public loginService:loginService, public statusBar: StatusBar,public Settings:Config, public splashScreen: SplashScreen,public sendtoserver:sent_to_server_service, private iab: InAppBrowser) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
        { title: 'ראשי', component: HomePage, icon:'ios-home-outline' },
        { title: 'המלץ לחבר', component: SuggestPage, icon:'ios-people-outline' },
        { title: 'פרטים אישיים', component: ProfilePage, icon:'ios-contact-outline' },
        { title: 'שאלות ותשובות', component: FaqPage, icon:'ios-help-outline' },
        { title: 'החשבון שלי', component: MyAccountPage, icon:'ios-card-outline' },
        { title: 'מי אנחנו', component: AboutPage, icon:'ios-information-circle-outline' },
        { title: 'תנאי שימוש',  component: 'TermsPage' , icon:'ios-checkbox-outline' },
        {title: 'התנתק', component: 'LogOut' , icon:'ios-close-circle-outline'}
    ];
     console.log("CustomerId:", this.Settings.CustomerId);
      console.log("Local : ", window.localStorage.id, window.localStorage.sid)
      if (window.localStorage.id == undefined || window.localStorage.id == '')
          this.rootPage = RegisterPage; //RegisterPage;//
      else
          this.rootPage = HomePage; // ChatPage; //

  }


    
    initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.getCustumerServerData();

        if (this.platform.is('ios')
            || this.platform.is('android')
            || this.platform.is('windows')) {
            //enableProdMode();
        }

    });
  }

    async getCustumerServerData()
    {
        await this.sendtoserver.getCustumerServerData('getCustumerServerData');
    }
    
    openPage(page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.component == 'LogOut') {
            window.localStorage.id = '';
            this.Settings.User = '';
            this.nav.setRoot(LoginPage);
            console.log("Out");
        }
        else if (page.component == 'TermsPage') {
            this.browser = this.iab.create(this.Settings.TermsURL, '_system', 'location=no,toolbar=yes');
        }
        else
            this.nav.push(page.component);
    }
}
