import {Component, NgZone, OnInit} from '@angular/core';
import {
    ActionSheetController, IonicPage, Loading, LoadingController, NavController, NavParams, Platform,
    ToastController
} from 'ionic-angular';
import {loginService} from "../../services/loginService";
import {toastService} from "../../services/toastService";
import {HomePage} from "../home/home";
import {LoginPage} from "../login/login";
import {Config} from "../../services/config";
import {sent_to_server_service} from "../../services/sent_to_server_service";


declare let cordova: any;

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-register',
    templateUrl: 'register.html',
})
export class RegisterPage implements OnInit {

    public info: Object = {
        name: '',
        phone: '',
        //address: '',
        info: '',
        password: '',
        repassword : '',
        mail: '',
        image: '',
        type: '',
        area: ''
    };

    public serverImage = '';
    public Regions: any[] = [];
    public Types: any[] = [];
    public emailregex;
    public base64Image: string;
    lastImage: string = null;
    loading: Loading;
    correctPath;
    currentName;
    public Type:Number;
    public CustomerId;
    public host;
    CustumerServerData:any = [];
    public imageLogo:any;

   

    constructor(public navCtrl: NavController, public navParams: NavParams,public zone: NgZone,public Login: loginService, public Toast: toastService,  public actionSheetCtrl: ActionSheetController, public toastCtrl: ToastController, public platform: Platform, public loadingCtrl: LoadingController,public Settings:Config,public sendtoserver:sent_to_server_service) {
        this.CustomerId = this.Settings.CustomerId;
        this.host = this.Settings.host;



        this.sendtoserver._JsonInfo.subscribe(val => {
            this.zone.run(() => {
                this.CustumerServerData = val;
                console.log("CustumerServerData",this.CustumerServerData)
                if (this.CustumerServerData) {
                    if (this.CustumerServerData.custumer_data) {
                        if (this.CustumerServerData.custumer_data.length > 0) {
                            this.imageLogo = this.host + this.CustumerServerData.custumer_data[0].image;
                        }
                    }
                }
            });
        });



    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad RegisterPage');
    }

    doRegister() {
        console.log("register response1: ");
        this.emailregex = /\S+@\S+\.\S+/;
        if (this.info['name'].length < 3)
            this.Toast.presentToast('הכנס שם מלא')
        else if (this.info['phone'].length < 8 || this.info['phone'].length > 10)
            this.Toast.presentToast('הכנס מספר טלפון חוקי')
        else if (!this.emailregex.test(this.info['mail'])) {
            this.info['mail'] = '';
            this.Toast.presentToast('מייל לא תקין')
        }
        //else if (this.info['address'].length < 2)
        //    this.Toast.presentToast('הכנס כתובת חוקית')
        else if (this.info['password'].length < 3)
            this.Toast.presentToast('הכנס סיסמה תקינה')

        else if (this.info['repassword'].length < 3)
            this.Toast.presentToast('הכנס וידוי סיסמה')

        else if (this.info['password'] != this.info['repassword']) {
            this.Toast.presentToast('וידוי סיסמה לא תואם לסיסמה שהזנת יש לתקן')
            this.info['repassword'] = '';
        }

        else {
            this.Login.RegisterUser("RegisterUser", this.info).then(data => {
                if (data == 0) {
                    this.Toast.presentToast('אימייל כבר בשימוש יש להזין מייל אחר');
                    this.info['mail'] = '';
                }
                else {
                    window.localStorage.id = data;
                    window.localStorage.name = this.info['name'];
                    this.navCtrl.setRoot(HomePage);
                }
            });
        }
    }

    GoToLogin() {
        this.navCtrl.push(LoginPage);
    }

    async ngOnInit() {


    }

    /// Image Upload
    public presentToast(text) {
        console.log("f3");
        let toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'bottom',
            cssClass: "ToastClass"
        });
        toast.present();
    }

}
