import { Component,NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {loginService} from "../../services/loginService";
import {HomePage} from "../home/home";
import {Config} from "../../services/config";
import {sent_to_server_service} from "../../services/sent_to_server_service";
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
    public Details = '';
    public CustomerId;
    public host;
    CustumerServerData:any = [];
    public imageLogo:any;
    public personal_details_video:any;


  constructor(public navCtrl: NavController,public Login:loginService, public navParams: NavParams,public sendtoserver:sent_to_server_service,public zone: NgZone, public Settings:Config,private domSanitizer: DomSanitizer) {

      this.CustomerId = this.Settings.CustomerId;
      this.host = this.Settings.host;
      this.sendtoserver._JsonInfo.subscribe(val => {
          this.zone.run(() => {
              this.CustumerServerData = val;
              console.log("CustumerServerData",this.CustumerServerData)
              if (this.CustumerServerData) {
                  if (this.CustumerServerData.custumer_data) {
                      if (this.CustumerServerData.custumer_data.length > 0) {
                          this.imageLogo = this.host + this.CustumerServerData.custumer_data[0].image;
                          this.personal_details_video = this.CustumerServerData.custumer_data[0].personal_details_video+'?rel=0';;
                          this.personal_details_video = this.domSanitizer.bypassSecurityTrustResourceUrl(this.personal_details_video);
                      }
                  }
              }
          });
      });


      this.Login.getUserById('getUserById').then(
          (data: any) => {
              this.Details = data[0];
    
              console.log("getUserById : " , this.Details);
          });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }
  
  saveDetails()
  {
      this.Login.SaveDetails('SaveProfile',this.Details).then(
          (data: any) => {
              console.log("SaveProfile11 : " , data);
              this.navCtrl.push(HomePage);
          });
  }

}
