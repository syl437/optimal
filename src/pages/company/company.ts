import {AfterViewInit, Component, ElementRef, NgZone, Pipe, Renderer, ViewChild,HostListener } from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {Config} from "../../services/config";
import {DomSanitizer} from "@angular/platform-browser";
import {sent_to_server_service} from "../../services/sent_to_server_service";
import { InAppBrowser } from '@ionic-native/in-app-browser';


/**
 * Generated class for the CompanyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()

@Component({
  selector: 'page-company',
  templateUrl: 'company.html',
})
@Pipe({name: 'replaceLineBreaks'})


export class CompanyPage  {

    public CustomerId;
    public host;
    CustumerServerData:any = [];
    public imageLogo:any;
    public companytitle:any;
    public companytext:any;
    public companyvideo:any;
    public escapedLink:any = '';
    public browser;


    constructor(public navCtrl: NavController, public navParams: NavParams,public sendtoserver:sent_to_server_service,public zone: NgZone , public Settings:Config,private domSanitizer: DomSanitizer,private sanitizer: DomSanitizer,elementRef: ElementRef, renderer: Renderer, private iab: InAppBrowser) {
      this.CustomerId = this.Settings.CustomerId;
      this.host = this.Settings.host;
      this.sendtoserver._JsonInfo.subscribe(val => {
          this.zone.run(() => {
              this.CustumerServerData = val;
              console.log("CustumerServerData",this.CustumerServerData)
              if (this.CustumerServerData) {
                  if (this.CustumerServerData.custumer_data) {
                      if (this.CustumerServerData.custumer_data.length > 0) {
                          this.imageLogo = this.host + this.CustumerServerData.custumer_data[0].image;
                      }
                      if (this.CustumerServerData.app_settings.length > 0) {
                          this.companytitle =  this.CustumerServerData.app_settings[0].company_title;
                          this.companytext =  this.CustumerServerData.app_settings[0].company_description;
                          this.companyvideo = this.CustumerServerData.app_settings[0].company_video+'?rel=0';;
                          this.companyvideo = this.domSanitizer.bypassSecurityTrustResourceUrl(this.companyvideo);
                      }
                  }
              }
          });
      });


        renderer.listen(elementRef.nativeElement, 'click', (event) => {
             //console.log(event.path[0].innerText);
            if (event.path[0].innerText =="תנאי השימוש") {
                this.browser = this.iab.create(this.Settings.TermsURL, '_system', 'location=no,toolbar=yes');
            }
        })
    }

    highlight(){
        return this.companytext.replace(new RegExp("תנאי השימוש", "gi"), match => {
            //return  this.sanitizer.bypassSecurityTrustHtml(this.companytext);
            return '<span class="highlightText" >תנאי השימוש</span>';
        });
    }



  ionViewDidLoad() {
    console.log('ionViewDidLoad CompanyPage');
  }

}
