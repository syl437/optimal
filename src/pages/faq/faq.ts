import { Component,NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {sent_to_server_service} from "../../services/sent_to_server_service";
import {Config} from "../../services/config";
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
/**
 * Generated class for the FaqPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-faq',
  templateUrl: 'faq.html',
})
export class FaqPage {

    public CustomerId;
    public host;
    CustumerServerData:any = [];
    public imageLogo:any;
    public faqvideo:any;
    public faqArray:any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,public sendtoserver:sent_to_server_service,public zone: NgZone , public Settings:Config,private domSanitizer: DomSanitizer) {
      this.CustomerId = this.Settings.CustomerId;
      this.host = this.Settings.host;
      this.sendtoserver._JsonInfo.subscribe(val => {
          this.zone.run(() => {
              this.CustumerServerData = val;
              console.log("CustumerServerData",this.CustumerServerData)
              if (this.CustumerServerData) {
                  if (this.CustumerServerData.custumer_data) {
                      if (this.CustumerServerData.custumer_data.length > 0) {
                          this.imageLogo = this.host + this.CustumerServerData.custumer_data[0].image;
                          this.faqvideo = this.CustumerServerData.custumer_data[0].faq_video+'?rel=0';;
                          this.faqvideo = this.domSanitizer.bypassSecurityTrustResourceUrl(this.faqvideo);
                      }
                  }

                  if (this.CustumerServerData.customers_questions_answers) {
                      if (this.CustumerServerData.customers_questions_answers.length > 0) {
                        this.faqArray = this.CustumerServerData.customers_questions_answers;
                      }
                  }
              }
          });
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FaqPage');
  }

}
