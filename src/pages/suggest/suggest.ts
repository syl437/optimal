import { Component,NgZone } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import {HomePage} from "../home/home";
import {loginService} from "../../services/loginService";
import {toastService} from "../../services/toastService";
import {sent_to_server_service} from "../../services/sent_to_server_service";
import {Config} from "../../services/config";
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { Keyboard } from '@ionic-native/keyboard';

/**
 * Generated class for the SuggestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-suggest',
  templateUrl: 'suggest.html',
})
export class SuggestPage {
    
    public info: Object = {
        name: '',
        phone: '',
        info: '',
        uid :window.localStorage.id
    };

    public keyboardHide:boolean = true;
    public CustomerId;
    public host;
    CustumerServerData:any = [];
    public imageLogo:any;
    public suggestvideo:any;

  constructor(public navCtrl: NavController,private alertCtrl: AlertController, public Toast: toastService, public Login:loginService , public navParams: NavParams,public sendtoserver:sent_to_server_service,public zone: NgZone , public Settings:Config,private domSanitizer: DomSanitizer,private keyboard: Keyboard,public platform: Platform) {
      this.CustomerId = this.Settings.CustomerId;
      this.host = this.Settings.host;
      //this.keyboardHide = this.keyboard.onKeyboardHide();
      //this.keyboardHide = false;
      this.sendtoserver._JsonInfo.subscribe(val => {
          this.zone.run(() => {
              this.CustumerServerData = val;
              console.log("CustumerServerData",this.CustumerServerData)
              if (this.CustumerServerData) {
                  if (this.CustumerServerData.custumer_data) {
                      if (this.CustumerServerData.custumer_data.length > 0) {
                          this.imageLogo = this.host + this.CustumerServerData.custumer_data[0].image;
                          this.suggestvideo = this.CustumerServerData.custumer_data[0].recommend_friend_video+'?rel=0';;
                          this.suggestvideo = this.domSanitizer.bypassSecurityTrustResourceUrl(this.suggestvideo);
                      }
                  }
              }
          });
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SuggestPage');
  }

    ionViewDidEnter() {

            this.keyboard.onKeyboardShow().subscribe(() => {
              this.keyboardHide = false;
          })
          this.keyboard.onKeyboardHide().subscribe(() => {
              this.keyboardHide = true;
          })
    }
    
    sendMessageToFriend()
    {
        if (this.info['name'].length < 3)
            this.Toast.presentToast('הכנס שם מלא')
        else if (this.info['phone'].length < 8 || this.info['phone'].length > 10)
            this.Toast.presentToast('הכנס מספר טלפון תקין')
        else
        {
            this.Login.sendRecommend('sendRecommend',this.info).then(
                (data: any) => {


                    console.log("sendRecommend : ",data);

                    let alertTitle = '';
                    let alertDesc = '';

                    if (data.status == 0) {
                        alertTitle = 'מספר טלפון קיים במערכת';
                        alertDesc = 'מספר הטלפון שהזנת כבר קיים במערכת יש להזין טלפון אחר';
                        this.info['phone'] = '';
                    }
                    else {
                        alertTitle = 'הודעתך לחבר נשלחה בהצלחה';
                        alertDesc = 'מעקב יופיע בעמודך האישי';
                    }

                    let alert = this.alertCtrl.create({
                        title: alertTitle,
                        subTitle: alertDesc,
                        cssClass: 'alertCss',
                        buttons: ['סגור']
                    });
                    alert.present();

                    if (data.status !="0")
                        this.navCtrl.setRoot(HomePage);

                });
        }
    }
}
