webpackJsonp([9],{

/***/ 116:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_sent_to_server_service__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_config__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__ = __webpack_require__(23);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AboutPage = (function () {
    function AboutPage(navCtrl, navParams, sendtoserver, zone, Settings, domSanitizer) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.sendtoserver = sendtoserver;
        this.zone = zone;
        this.Settings = Settings;
        this.domSanitizer = domSanitizer;
        this.CustumerServerData = [];
        this.CustomerId = this.Settings.CustomerId;
        this.host = this.Settings.host;
        this.sendtoserver._JsonInfo.subscribe(function (val) {
            _this.zone.run(function () {
                _this.CustumerServerData = val;
                console.log("CustumerServerData", _this.CustumerServerData);
                if (_this.CustumerServerData) {
                    if (_this.CustumerServerData.custumer_data) {
                        if (_this.CustumerServerData.custumer_data.length > 0) {
                            _this.imageLogo = _this.host + _this.CustumerServerData.custumer_data[0].image;
                            _this.abouttitle = _this.CustumerServerData.custumer_data[0].about_title;
                            _this.abouttext = _this.CustumerServerData.custumer_data[0].about_description;
                            _this.aboutvideo = _this.CustumerServerData.custumer_data[0].about_us_video + '?rel=0';
                            ;
                            _this.aboutvideo = _this.domSanitizer.bypassSecurityTrustResourceUrl(_this.aboutvideo);
                        }
                    }
                }
            });
        });
    }
    AboutPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AboutPage');
    };
    AboutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-about',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\baloon\src\pages\about\about.html"*/'<ion-header>\n\n    <ion-navbar no-border-top class="ToolBarClass">\n\n        <button ion-button menuToggle end="">\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>\n\n            <div style="width: 100%">\n\n                <img [src]="imageLogo" class="headLogo1"/>\n\n            </div>\n\n        </ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n    <div class="Video">\n\n        <iframe width="100%" height="230" [src]="aboutvideo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>\n\n    </div>\n\n    <div>\n\n        <h1 class="TitleText"  [innerHTML]="abouttitle"></h1>\n\n        <h3 class="DescText"  [innerHTML]="abouttext"></h3>\n\n\n\n\n\n    </div>\n\n</ion-content>\n\n<ion-footer style="padding: 0px; margin: 0px; background-color: #144e6e">\n\n    <footer></footer>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Desktop\github\baloon\src\pages\about\about.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_sent_to_server_service__["a" /* sent_to_server_service */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__["c" /* DomSanitizer */]])
    ], AboutPage);
    return AboutPage;
}());

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 117:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FaqPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_sent_to_server_service__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_config__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__ = __webpack_require__(23);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the FaqPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FaqPage = (function () {
    function FaqPage(navCtrl, navParams, sendtoserver, zone, Settings, domSanitizer) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.sendtoserver = sendtoserver;
        this.zone = zone;
        this.Settings = Settings;
        this.domSanitizer = domSanitizer;
        this.CustumerServerData = [];
        this.faqArray = [];
        this.CustomerId = this.Settings.CustomerId;
        this.host = this.Settings.host;
        this.sendtoserver._JsonInfo.subscribe(function (val) {
            _this.zone.run(function () {
                _this.CustumerServerData = val;
                console.log("CustumerServerData", _this.CustumerServerData);
                if (_this.CustumerServerData) {
                    if (_this.CustumerServerData.custumer_data) {
                        if (_this.CustumerServerData.custumer_data.length > 0) {
                            _this.imageLogo = _this.host + _this.CustumerServerData.custumer_data[0].image;
                            _this.faqvideo = _this.CustumerServerData.custumer_data[0].faq_video + '?rel=0';
                            ;
                            _this.faqvideo = _this.domSanitizer.bypassSecurityTrustResourceUrl(_this.faqvideo);
                        }
                    }
                    if (_this.CustumerServerData.customers_questions_answers) {
                        if (_this.CustumerServerData.customers_questions_answers.length > 0) {
                            _this.faqArray = _this.CustumerServerData.customers_questions_answers;
                        }
                    }
                }
            });
        });
    }
    FaqPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FaqPage');
    };
    FaqPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-faq',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\baloon\src\pages\faq\faq.html"*/'<ion-header>\n\n    <ion-navbar no-border-top class="ToolBarClass">\n\n        <button ion-button menuToggle end="">\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>\n\n            <div style="width: 100%">\n\n                <img [src]="imageLogo" class="headLogo1"/>\n\n            </div>\n\n        </ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n    <div class="Video">\n\n        <iframe width="100%" height="230" [src]="faqvideo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>\n\n    </div>\n\n    <div *ngFor="let row of faqArray">\n\n        <h1 class="TitleText" [innerHTML]="row.question"></h1>\n\n        <h3 class="DescText" [innerHTML]="row.answer"></h3>\n\n    </div>\n\n</ion-content>\n\n<ion-footer style="padding: 0px; margin: 0px; background-color: #144e6e">\n\n    <footer></footer>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Desktop\github\baloon\src\pages\faq\faq.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_sent_to_server_service__["a" /* sent_to_server_service */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__["c" /* DomSanitizer */]])
    ], FaqPage);
    return FaqPage;
}());

//# sourceMappingURL=faq.js.map

/***/ }),

/***/ 118:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyAccountPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_loginService__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_sent_to_server_service__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_config__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__profile_profile__ = __webpack_require__(86);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the MyAccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var MyAccountPage = (function () {
    function MyAccountPage(navCtrl, navParams, Login, sendtoserver, zone, Settings, domSanitizer, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Login = Login;
        this.sendtoserver = sendtoserver;
        this.zone = zone;
        this.Settings = Settings;
        this.domSanitizer = domSanitizer;
        this.alertCtrl = alertCtrl;
        this.sum = 0;
        this.CustumerServerData = [];
        this.CustomerId = this.Settings.CustomerId;
        this.host = this.Settings.host;
        this.sendtoserver._JsonInfo.subscribe(function (val) {
            _this.zone.run(function () {
                _this.CustumerServerData = val;
                console.log("CustumerServerData", _this.CustumerServerData);
                if (_this.CustumerServerData) {
                    if (_this.CustumerServerData.custumer_data) {
                        if (_this.CustumerServerData.custumer_data.length > 0) {
                            _this.imageLogo = _this.host + _this.CustumerServerData.custumer_data[0].image;
                            _this.myaccountvideo = _this.CustumerServerData.custumer_data[0].my_account_video + '?rel=0';
                            ;
                            _this.myaccountvideo = _this.domSanitizer.bypassSecurityTrustResourceUrl(_this.myaccountvideo);
                        }
                    }
                }
            });
        });
        this.Login.getRecommendById('getRecommendById').then(function (data) {
            console.log("getRecommendById : ", data);
            _this.recommendArray = data;
        });
    }
    MyAccountPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MyAccountPage');
    };
    MyAccountPage.prototype.requestWithdraw = function () {
        var _this = this;
        this.Login.requestWithdraw('requestWithdraw').then(function (data) {
            var alertTitle = '';
            console.log("status: ", data.status);
            if (data.status == 1) {
                alertTitle = 'בקשתך למשיכה נשלחה בהצלחה';
            }
            if (data.status == 0) {
                alertTitle = 'בקשתך התקבלה ובתהליך בדיקה';
            }
            if (data.status == 2) {
                alertTitle = 'יש תחילה למלא את כל שדות פרטי בנק בפרטים אישיים';
            }
            console.log("requestWithdraw : ", data);
            var alert = _this.alertCtrl.create({
                title: alertTitle,
                //subTitle: 'מעקב יופיע בעמודך האישי',
                cssClass: 'alertCss',
                buttons: ['סגור']
            });
            alert.present();
            if (data.status != 2)
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
            else
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__profile_profile__["a" /* ProfilePage */]);
        });
    };
    MyAccountPage.prototype.calculateSum = function () {
        console.log(typeof this.recommendArray);
        this.sum = 0;
        if (this.recommendArray) {
            for (var i = 0; i < this.recommendArray.length; i++)
                this.sum += Number(this.recommendArray[i].deal_price);
            return this.sum;
        }
        else
            return 0;
    };
    MyAccountPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-my-account',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\baloon\src\pages\my-account\my-account.html"*/'\n\n<ion-header>\n\n    <ion-navbar no-border-top class="ToolBarClass">\n\n        <button ion-button menuToggle end="">\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>\n\n            <div style="width: 100%">\n\n                <img [src]="imageLogo" class="headLogo1"/>\n\n            </div>\n\n        </ion-title>\n\n\n\n\n\n        <ion-buttons  end >\n\n            <button (click)="requestWithdraw()" *ngIf="calculateSum() >= 500" ion-button icon-only>\n\n                <ion-icon name="logo-usd"></ion-icon>\n\n            </button>\n\n        </ion-buttons>\n\n\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n    <div class="Video">\n\n        <iframe width="100%" height="230" [src]="myaccountvideo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>\n\n    </div>\n\n    <div>\n\n        <div *ngFor="let recommend of recommendArray let i=index" >\n\n            <!---    Row ----- -->\n\n            <div class="accountRow" >\n\n                <div class="accountImage" align="center">\n\n                    <div style="width: 80%">\n\n                        <img src="images/avatar/1.png" style="width: 100%;"/>\n\n                    </div>\n\n                </div>\n\n                <div class="accountTitle">\n\n                    <p class="accountTitleName">{{recommend.name}}</p>\n\n                    <p class="accountTitlePhone">{{recommend.phone}}</p>\n\n                </div>\n\n                <div class="accountPrice">\n\n                    {{recommend.deal_price}} ש"ח\n\n                </div>\n\n            </div>\n\n            <div class="rowLine"></div>\n\n        </div>\n\n    </div>\n\n</ion-content>\n\n<ion-footer align="center" no-border>\n\n   <div class="footerPrice">\n\n       סה"כ {{calculateSum()}} ש"ח\n\n   </div>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Desktop\github\baloon\src\pages\my-account\my-account.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__services_loginService__["a" /* loginService */], __WEBPACK_IMPORTED_MODULE_4__services_sent_to_server_service__["a" /* sent_to_server_service */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_5__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["c" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], MyAccountPage);
    return MyAccountPage;
}());

//# sourceMappingURL=my-account.js.map

/***/ }),

/***/ 119:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SuggestPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_loginService__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_toastService__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_sent_to_server_service__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_config__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_keyboard__ = __webpack_require__(375);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the SuggestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SuggestPage = (function () {
    function SuggestPage(navCtrl, alertCtrl, Toast, Login, navParams, sendtoserver, zone, Settings, domSanitizer, keyboard, platform) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.Toast = Toast;
        this.Login = Login;
        this.navParams = navParams;
        this.sendtoserver = sendtoserver;
        this.zone = zone;
        this.Settings = Settings;
        this.domSanitizer = domSanitizer;
        this.keyboard = keyboard;
        this.platform = platform;
        this.info = {
            name: '',
            phone: '',
            info: '',
            uid: window.localStorage.id
        };
        this.keyboardHide = true;
        this.CustumerServerData = [];
        this.CustomerId = this.Settings.CustomerId;
        this.host = this.Settings.host;
        //this.keyboardHide = this.keyboard.onKeyboardHide();
        //this.keyboardHide = false;
        this.sendtoserver._JsonInfo.subscribe(function (val) {
            _this.zone.run(function () {
                _this.CustumerServerData = val;
                console.log("CustumerServerData", _this.CustumerServerData);
                if (_this.CustumerServerData) {
                    if (_this.CustumerServerData.custumer_data) {
                        if (_this.CustumerServerData.custumer_data.length > 0) {
                            _this.imageLogo = _this.host + _this.CustumerServerData.custumer_data[0].image;
                            _this.suggestvideo = _this.CustumerServerData.custumer_data[0].recommend_friend_video + '?rel=0';
                            ;
                            _this.suggestvideo = _this.domSanitizer.bypassSecurityTrustResourceUrl(_this.suggestvideo);
                        }
                    }
                }
            });
        });
    }
    SuggestPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SuggestPage');
    };
    SuggestPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.keyboard.onKeyboardShow().subscribe(function () {
            _this.keyboardHide = false;
        });
        this.keyboard.onKeyboardHide().subscribe(function () {
            _this.keyboardHide = true;
        });
    };
    SuggestPage.prototype.sendMessageToFriend = function () {
        var _this = this;
        if (this.info['name'].length < 3)
            this.Toast.presentToast('הכנס שם מלא');
        else if (this.info['phone'].length < 8 || this.info['phone'].length > 10)
            this.Toast.presentToast('הכנס מספר טלפון תקין');
        else {
            this.Login.sendRecommend('sendRecommend', this.info).then(function (data) {
                console.log("sendRecommend : ", data);
                var alertTitle = '';
                var alertDesc = '';
                if (data.status == 0) {
                    alertTitle = 'מספר טלפון קיים במערכת';
                    alertDesc = 'מספר הטלפון שהזנת כבר קיים במערכת יש להזין טלפון אחר';
                    _this.info['phone'] = '';
                }
                else {
                    alertTitle = 'הודעתך לחבר נשלחה בהצלחה';
                    alertDesc = 'מעקב יופיע בעמודך האישי';
                }
                var alert = _this.alertCtrl.create({
                    title: alertTitle,
                    subTitle: alertDesc,
                    cssClass: 'alertCss',
                    buttons: ['סגור']
                });
                alert.present();
                if (data.status != "0")
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
            });
        }
    };
    SuggestPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-suggest',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\baloon\src\pages\suggest\suggest.html"*/'<ion-header>\n\n    <ion-navbar no-border-top class="ToolBarClass">\n\n        <button ion-button menuToggle end="">\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>\n\n            <div style="width: 100%">\n\n                <img [src]="imageLogo" class="headLogo1"/>\n\n            </div>\n\n        </ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n    <!--\n\n    <div class="Video" *ngIf="keyboardHide">\n\n        <iframe width="100%" height="230" [src]="suggestvideo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>\n\n    </div>\n\n     -->\n\n    <div align="center">\n\n        <!--<div class="friendIcon">-->\n\n            <!--<img src="images/suggest.png" style="width: 100%"/>-->\n\n        <!--</div>-->\n\n\n\n        <div class="InputText">\n\n            <input type="text" [(ngModel)]="info.name" class="mt10" placeholder="הכנס את שם החבר">\n\n        </div>\n\n\n\n        <div class="InputText">\n\n            <input [(ngModel)]="info.phone" type="tel" class="mt10" placeholder="הכנס מספר טלפון של החבר">\n\n        </div>\n\n\n\n        <div class="InputText mt10">\n\n            <textarea [(ngModel)]="info.info" name="info" placeholder="פרטים נוספים" rows="4"></textarea>\n\n        </div>\n\n\n\n        <button ion-button class="suggestSendButton" (click)="sendMessageToFriend()">שלח לחבר</button>\n\n    </div>\n\n</ion-content>\n\n\n\n\n\n<ion-footer style="padding: 0px; margin: 0px; background-color: #144e6e">\n\n    <footer></footer>\n\n</ion-footer>\n\n'/*ion-inline-end:"C:\Users\USER\Desktop\github\baloon\src\pages\suggest\suggest.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__services_toastService__["a" /* toastService */], __WEBPACK_IMPORTED_MODULE_3__services_loginService__["a" /* loginService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_5__services_sent_to_server_service__["a" /* sent_to_server_service */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_6__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser__["c" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_keyboard__["a" /* Keyboard */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */]])
    ], SuggestPage);
    return SuggestPage;
}());

//# sourceMappingURL=suggest.js.map

/***/ }),

/***/ 120:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_loginService__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_toastService__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_config__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_sent_to_server_service__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RegisterPage = (function () {
    function RegisterPage(navCtrl, navParams, zone, Login, Toast, actionSheetCtrl, toastCtrl, platform, loadingCtrl, Settings, sendtoserver) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.zone = zone;
        this.Login = Login;
        this.Toast = Toast;
        this.actionSheetCtrl = actionSheetCtrl;
        this.toastCtrl = toastCtrl;
        this.platform = platform;
        this.loadingCtrl = loadingCtrl;
        this.Settings = Settings;
        this.sendtoserver = sendtoserver;
        this.info = {
            name: '',
            phone: '',
            //address: '',
            info: '',
            password: '',
            repassword: '',
            mail: '',
            image: '',
            type: '',
            area: ''
        };
        this.serverImage = '';
        this.Regions = [];
        this.Types = [];
        this.lastImage = null;
        this.CustumerServerData = [];
        this.CustomerId = this.Settings.CustomerId;
        this.host = this.Settings.host;
        this.sendtoserver._JsonInfo.subscribe(function (val) {
            _this.zone.run(function () {
                _this.CustumerServerData = val;
                console.log("CustumerServerData", _this.CustumerServerData);
                if (_this.CustumerServerData) {
                    if (_this.CustumerServerData.custumer_data) {
                        if (_this.CustumerServerData.custumer_data.length > 0) {
                            _this.imageLogo = _this.host + _this.CustumerServerData.custumer_data[0].image;
                        }
                    }
                }
            });
        });
    }
    RegisterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegisterPage');
    };
    RegisterPage.prototype.doRegister = function () {
        var _this = this;
        console.log("register response1: ");
        this.emailregex = /\S+@\S+\.\S+/;
        if (this.info['name'].length < 3)
            this.Toast.presentToast('הכנס שם מלא');
        else if (this.info['phone'].length < 8 || this.info['phone'].length > 10)
            this.Toast.presentToast('הכנס מספר טלפון חוקי');
        else if (!this.emailregex.test(this.info['mail'])) {
            this.info['mail'] = '';
            this.Toast.presentToast('מייל לא תקין');
        }
        else if (this.info['password'].length < 3)
            this.Toast.presentToast('הכנס סיסמה תקינה');
        else if (this.info['repassword'].length < 3)
            this.Toast.presentToast('הכנס וידוי סיסמה');
        else if (this.info['password'] != this.info['repassword']) {
            this.Toast.presentToast('וידוי סיסמה לא תואם לסיסמה שהזנת יש לתקן');
            this.info['repassword'] = '';
        }
        else {
            this.Login.RegisterUser("RegisterUser", this.info).then(function (data) {
                if (data == 0) {
                    _this.Toast.presentToast('אימייל כבר בשימוש יש להזין מייל אחר');
                    _this.info['mail'] = '';
                }
                else {
                    window.localStorage.id = data;
                    window.localStorage.name = _this.info['name'];
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
                }
            });
        }
    };
    RegisterPage.prototype.GoToLogin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */]);
    };
    RegisterPage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    /// Image Upload
    RegisterPage.prototype.presentToast = function (text) {
        console.log("f3");
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'bottom',
            cssClass: "ToastClass"
        });
        toast.present();
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-register',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\baloon\src\pages\register\register.html"*/'<ion-content padding class="loginMain">\n\n  <div >\n\n      <div style="width: 80%; margin:auto; margin-top: 40px;">\n\n          <img [src]="imageLogo" style="width: 100%" />\n\n      </div>\n\n    <div class="InputsText" align="center">\n\n      <div class="InputText">\n\n        <input type="text" placeholder="הכנס שם מלא" [(ngModel)]="info.name" />\n\n      </div>\n\n      <div class="InputText mt10">\n\n        <input type="tel" placeholder="הכנס מספר טלפון" [(ngModel)]="info.phone" />\n\n      </div>\n\n      <!--\n\n      <div class="InputText mt10">\n\n        <input type="text" placeholder="הכנס כתובת מדוייקת" [(ngModel)]="info.address" />\n\n      </div>\n\n      -->\n\n      <div class="InputText mt10">\n\n        <input type="email" placeholder="הכנס אימייל" [(ngModel)]="info.mail" />\n\n      </div>\n\n      <div class="InputText mt10">\n\n        <input type="password" placeholder="הכנס סיסמה" [(ngModel)]="info.password" />\n\n      </div>\n\n\n\n      <div class="InputText mt10">\n\n        <input type="password" placeholder="וידוי סיסמה" [(ngModel)]="info.repassword" />\n\n      </div>\n\n\n\n      <button class="loginButton" ion-button block (click)="doRegister()">התחל</button>\n\n      <a class="register" (click)="GoToLogin()">יש לך כבר סיסמה ? התחבר למערכת </a>\n\n    </div>\n\n  </div>\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\USER\Desktop\github\baloon\src\pages\register\register.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_2__services_loginService__["a" /* loginService */], __WEBPACK_IMPORTED_MODULE_3__services_toastService__["a" /* toastService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_6__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_7__services_sent_to_server_service__["a" /* sent_to_server_service */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 21:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Config; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};


var Config = (function () {
    function Config() {
        this.CustomerId = '1';
        this.UserId = '';
        this.PushId = '';
        this.ServerUrl = 'http://tapper.org.il/baloon/laravel/public/api/';
        this.host = 'http://tapper.org.il/baloon/laravel/storage/app/public/';
        this.TermsURL = 'http://tapper.org.il/baloon/terms.pdf';
        this.CourseId = '';
        this.User = '';
        this.Substitute = 0;
        this.JobsCount = 0;
        this.openJobs = 2;
        this.openJobsType3 = 3;
        this.openJobsType4 = 4;
        this.TotalSub3 = 3;
        this.TotalSub4 = 4;
        this.Messages = 0;
        this.Regions = [];
        this.Types = [];
        this.CustumerServerData = [];
    }
    ;
    Config.prototype.SetCustumerServerData = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.CustumerServerData = data;
                return [2 /*return*/];
            });
        });
    };
    Config.prototype.SetUserPush = function (push_id) {
        this.PushId = push_id;
        window.localStorage.push_id = push_id;
    };
    Config.prototype.getTypeName = function (id) {
        var name = '';
        if (id == 1)
            name = "גננת";
        if (id == 3)
            name = "סייעת";
        if (id == 3)
            name = "גננת מחליפה";
        if (id == 4)
            name = "סייעת מחליפה";
        return name;
    };
    Config = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], Config);
    return Config;
}());

;
//# sourceMappingURL=config.js.map

/***/ }),

/***/ 217:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 217;

/***/ }),

/***/ 25:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return sent_to_server_service; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_BehaviorSubject__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_BehaviorSubject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ngx_restangular__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var sent_to_server_service = (function () {
    function sent_to_server_service(http, Settings, restangular, loadingCtrl) {
        this.http = http;
        this.Settings = Settings;
        this.restangular = restangular;
        this.loadingCtrl = loadingCtrl;
        this._JsonInfo = new __WEBPACK_IMPORTED_MODULE_4_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.JsonInfo$ = this._JsonInfo.asObservable();
        this.ServerUrl = Settings.ServerUrl;
        this.CustomerId = Settings.CustomerId;
    }
    Object.defineProperty(sent_to_server_service.prototype, "JsonInfo", {
        get: function () { return this._JsonInfo.getValue(); },
        enumerable: true,
        configurable: true
    });
    ;
    sent_to_server_service.prototype.getCustumerServerData = function (url) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var loading, body;
            return __generator(this, function (_a) {
                loading = this.loadingCtrl.create({ content: 'Please wait...' });
                loading.present();
                body = new FormData();
                body.append('customer_id', this.CustomerId);
                return [2 /*return*/, this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res; }).do(function (data) {
                        console.log("Notes : ", data.json());
                        _this._JsonInfo.next(data.json());
                        loading.dismiss();
                    }).toPromise()];
            });
        });
    };
    sent_to_server_service.prototype.addNote = function (url, info) {
        var body = new FormData();
        body.append('customer_id', this.CustomerId);
        body.append('title', info["title"]);
        body.append('info', info["info"]);
        body.append('uid', window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res; }).do(function (data) { console.log("Notes : ", data); }).toPromise();
    };
    sent_to_server_service.prototype.updateNote = function (url, info, id) {
        var body = new FormData();
        body.append('customer_id', this.CustomerId);
        body.append('title', info["title"]);
        body.append('info', info["info"]);
        body.append('id', id);
        body.append('uid', window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res; }).do(function (data) { console.log("Notes : ", data); }).toPromise();
    };
    sent_to_server_service.prototype.getAllNotes = function (url) {
        var body = new FormData();
        body.append('uid', window.localStorage.id);
        body.append('customer_id', this.CustomerId);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res; }).do(function (data) { console.log("Notes : ", data); }).toPromise();
    };
    sent_to_server_service.prototype.deleteNote = function (url, id) {
        var body = new FormData();
        body.append('id', id);
        body.append('uid', window.localStorage.id);
        body.append('customer_id', this.CustomerId);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res; }).do(function (data) { console.log("Notes : ", data); }).toPromise();
    };
    //Bid
    sent_to_server_service.prototype.addBid = function (url, job, info) {
        var body = new FormData();
        body.append('job', job);
        body.append('info', info);
        body.append('uid', window.localStorage.id);
        body.append('customer_id', this.CustomerId);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res; }).do(function (data) { console.log("Notes : ", data); }).toPromise();
    };
    sent_to_server_service.prototype.getHomeDetails = function (url) {
        var body = new FormData();
        body.append('uid', window.localStorage.id);
        body.append('customer_id', this.CustomerId);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res; }).do(function (data) { console.log("Notes : ", data); }).toPromise();
    };
    sent_to_server_service.prototype.updateJob = function (url, info, id) {
        var body = new FormData();
        body.append('Date2', info['date']);
        body.append('Time2', info['time']);
        body.append('info', info['info']);
        body.append('id', id);
        body.append('uid', window.localStorage.id);
        body.append('customer_id', this.CustomerId);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res; }).do(function (data) { console.log("Notes : ", data); }).toPromise();
    };
    sent_to_server_service = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_5_ngx_restangular__["Restangular"],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["g" /* LoadingController */]])
    ], sent_to_server_service);
    return sent_to_server_service;
}());

;
//# sourceMappingURL=sent_to_server_service.js.map

/***/ }),

/***/ 261:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/about/about.module": [
		793,
		8
	],
	"../pages/company/company.module": [
		795,
		7
	],
	"../pages/faq/faq.module": [
		794,
		6
	],
	"../pages/forgotpass/forgotpass.module": [
		796,
		5
	],
	"../pages/login/login.module": [
		797,
		4
	],
	"../pages/my-account/my-account.module": [
		800,
		3
	],
	"../pages/profile/profile.module": [
		798,
		2
	],
	"../pages/register/register.module": [
		799,
		1
	],
	"../pages/suggest/suggest.module": [
		801,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 261;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 377:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForgotpassPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_sent_to_server_service__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_toastService__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_loginService__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_config__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__login_login__ = __webpack_require__(85);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the ForgotpassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ForgotpassPage = (function () {
    function ForgotpassPage(navCtrl, navParams, Login, Settings, Toast, toastCtrl, sendtoserver, zone) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Login = Login;
        this.Settings = Settings;
        this.Toast = Toast;
        this.toastCtrl = toastCtrl;
        this.sendtoserver = sendtoserver;
        this.zone = zone;
        this.info = { mail: "" };
        this.user = '';
        this.CustumerServerData = [];
        this.CustomerId = this.Settings.CustomerId;
        this.host = this.Settings.host;
        this.sendtoserver._JsonInfo.subscribe(function (val) {
            _this.zone.run(function () {
                _this.CustumerServerData = val;
                console.log("CustumerServerData", _this.CustumerServerData);
                if (_this.CustumerServerData) {
                    if (_this.CustumerServerData.custumer_data) {
                        if (_this.CustumerServerData.custumer_data.length > 0) {
                            _this.imageLogo = _this.host + _this.CustumerServerData.custumer_data[0].image;
                        }
                    }
                }
            });
        });
    }
    ForgotpassPage.prototype.sendPassword = function () {
        var _this = this;
        this.emailregex = /\S+@\S+\.\S+/;
        if (this.info['mail'].length == 0)
            this.Toast.presentToast('הכנס כתובת מייל');
        else if (!this.emailregex.test(this.info['mail'])) {
            this.info['mail'] = '';
            this.Toast.presentToast('מייל לא תקין');
        }
        else {
            this.Login.ForgotPass('ForgotPass', this.info).then(function (data) {
                if (data.status == 1) {
                    _this.Toast.presentToast('פרטי התחברות נשלחו למייל שהזנת');
                    _this.info['mail'] = '';
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__login_login__["a" /* LoginPage */]);
                }
                else {
                    _this.Toast.presentToast('המייל שהוזן לא קיים במערכת');
                    _this.info['mail'] = '';
                }
            });
        }
    };
    ForgotpassPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ForgotpassPage');
    };
    ForgotpassPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-forgotpass',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\baloon\src\pages\forgotpass\forgotpass.html"*/'\n<ion-header>\n  <ion-navbar no-border-top class="ToolBarClass">\n    <button ion-button menuToggle end="">\n      <ion-icon name="menu"></ion-icon>\n    </button>\n\n\n\n  </ion-navbar>\n</ion-header>\n\n\n\n<ion-content padding class="loginMain">\n  <div >\n    <div style="width: 80%; margin:auto; margin-top: 40px;">\n      <img [src]="imageLogo" style="width: 100%" />\n    </div>\n\n    <div class="InputsText" align="center">\n      <div class="InputText">\n        <input type="email" placeholder="הכנס מייל תקין" [(ngModel)]="info.mail" />\n      </div>\n\n\n      <button class="loginButton"  ion-button block (click)="sendPassword()">התחל</button>\n\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"C:\Users\USER\Desktop\github\baloon\src\pages\forgotpass\forgotpass.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__services_loginService__["a" /* loginService */], __WEBPACK_IMPORTED_MODULE_5__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_3__services_toastService__["a" /* toastService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ToastController */], __WEBPACK_IMPORTED_MODULE_2__services_sent_to_server_service__["a" /* sent_to_server_service */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"]])
    ], ForgotpassPage);
    return ForgotpassPage;
}());

//# sourceMappingURL=forgotpass.js.map

/***/ }),

/***/ 420:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CompanyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_config__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_sent_to_server_service__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_in_app_browser__ = __webpack_require__(111);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the CompanyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CompanyPage = (function () {
    function CompanyPage(navCtrl, navParams, sendtoserver, zone, Settings, domSanitizer, sanitizer, elementRef, renderer, iab) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.sendtoserver = sendtoserver;
        this.zone = zone;
        this.Settings = Settings;
        this.domSanitizer = domSanitizer;
        this.sanitizer = sanitizer;
        this.iab = iab;
        this.CustumerServerData = [];
        this.escapedLink = '';
        this.CustomerId = this.Settings.CustomerId;
        this.host = this.Settings.host;
        this.sendtoserver._JsonInfo.subscribe(function (val) {
            _this.zone.run(function () {
                _this.CustumerServerData = val;
                console.log("CustumerServerData", _this.CustumerServerData);
                if (_this.CustumerServerData) {
                    if (_this.CustumerServerData.custumer_data) {
                        if (_this.CustumerServerData.custumer_data.length > 0) {
                            _this.imageLogo = _this.host + _this.CustumerServerData.custumer_data[0].image;
                        }
                        if (_this.CustumerServerData.app_settings.length > 0) {
                            _this.companytitle = _this.CustumerServerData.app_settings[0].company_title;
                            _this.companytext = _this.CustumerServerData.app_settings[0].company_description;
                            _this.companyvideo = _this.CustumerServerData.app_settings[0].company_video + '?rel=0';
                            ;
                            _this.companyvideo = _this.domSanitizer.bypassSecurityTrustResourceUrl(_this.companyvideo);
                        }
                    }
                }
            });
        });
        renderer.listen(elementRef.nativeElement, 'click', function (event) {
            //console.log(event.path[0].innerText);
            if (event.path[0].innerText == "תנאי השימוש") {
                _this.browser = _this.iab.create(_this.Settings.TermsURL, '_system', 'location=no,toolbar=yes');
            }
        });
    }
    CompanyPage.prototype.highlight = function () {
        return this.companytext.replace(new RegExp("תנאי השימוש", "gi"), function (match) {
            //return  this.sanitizer.bypassSecurityTrustHtml(this.companytext);
            return '<span class="highlightText" >תנאי השימוש</span>';
        });
    };
    CompanyPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CompanyPage');
    };
    CompanyPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-company',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\baloon\src\pages\company\company.html"*/'<ion-header>\n  <ion-navbar no-border-top class="ToolBarClass">\n    <button ion-button menuToggle end="">\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>\n      <div style="width: 100%">\n        <img [src]="imageLogo" class="headLogo1"/>\n      </div>\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n  <div class="Video">\n    <iframe width="100%" height="230" [src]="companyvideo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>\n  </div>\n  <div>\n    <h1 class="TitleText"  [innerHTML]="companytitle"></h1>\n    <h3 class="DescText" [innerHTML]="highlight() "></h3>\n  </div>\n</ion-content>\n<ion-footer style="padding: 0px; margin: 0px; background-color: #144e6e">\n  <footer></footer>\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Desktop\github\baloon\src\pages\company\company.html"*/,
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({ name: 'replaceLineBreaks' }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__services_sent_to_server_service__["a" /* sent_to_server_service */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_2__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["c" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["c" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"], __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"], __WEBPACK_IMPORTED_MODULE_5__ionic_native_in_app_browser__["a" /* InAppBrowser */]])
    ], CompanyPage);
    return CompanyPage;
}());

//# sourceMappingURL=company.js.map

/***/ }),

/***/ 421:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WazeModalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_sent_to_server_service__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_config__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the WazeModalComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var WazeModalComponent = (function () {
    function WazeModalComponent(viewCtrl, sendtoserver, zone, Settings) {
        var _this = this;
        this.viewCtrl = viewCtrl;
        this.sendtoserver = sendtoserver;
        this.zone = zone;
        this.Settings = Settings;
        this.CustumerServerData = [];
        this.customers_branches = [];
        this.CustomerId = this.Settings.CustomerId;
        this.host = this.Settings.host;
        this.sendtoserver._JsonInfo.subscribe(function (val) {
            _this.zone.run(function () {
                _this.CustumerServerData = val;
                console.log("CustumerServerData", _this.CustumerServerData);
                if (_this.CustumerServerData) {
                    if (_this.CustumerServerData.customers_branches) {
                        if (_this.CustumerServerData.customers_branches.length > 0) {
                            _this.customers_branches = _this.CustumerServerData.customers_branches;
                        }
                    }
                }
            });
        });
    }
    WazeModalComponent.prototype.onSubmit = function (type) {
        this.viewCtrl.dismiss(type);
    };
    WazeModalComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'waze-modal',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\baloon\src\components\waze-modal\waze-modal.html"*/'\n\n\n\n<!-- Generated template for the WazeModalComponent component -->\n\n    <div class="Content" align="center">\n\n      <div class="buttons">\n\n        <button class="bt" *ngFor="let row of customers_branches" ion-button (click)="onSubmit(row)">{{row.name}}</button><br>\n\n      </div>\n\n    </div>\n\n'/*ion-inline-end:"C:\Users\USER\Desktop\github\baloon\src\components\waze-modal\waze-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__services_sent_to_server_service__["a" /* sent_to_server_service */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */]])
    ], WazeModalComponent);
    return WazeModalComponent;
}());

//# sourceMappingURL=waze-modal.js.map

/***/ }),

/***/ 422:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(423);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(427);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 427:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export RestangularConfigFactory */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(790);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_list_list__ = __webpack_require__(791);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__ = __webpack_require__(418);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__ = __webpack_require__(419);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_about_about__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_faq_faq__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_my_account_my_account__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_suggest_suggest__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_profile_profile__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_launch_navigator__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_social_sharing__ = __webpack_require__(183);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_login_login__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__services_loginService__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__services_config__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_register_register__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_company_company__ = __webpack_require__(420);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__services_toastService__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__angular_http__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__components_footer_footer__ = __webpack_require__(792);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__components_waze_modal_waze_modal__ = __webpack_require__(421);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__services_sent_to_server_service__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25_ngx_restangular__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_25_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_native_in_app_browser__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__ionic_native_keyboard__ = __webpack_require__(375);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_forgotpass_forgotpass__ = __webpack_require__(377);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__ionic_native_facebook__ = __webpack_require__(378);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






























function RestangularConfigFactory(RestangularProvider) {
    var _this = this;
    // Setting up the Restangular endpoint - where to send queries
    RestangularProvider.setBaseUrl("http://tapper.org.il/baloon/laravel/public/api/");
    // Every time the query is made, it's console.logged.
    // Restangular doesn't support boolean answers from the API, so right now null is returned
    RestangularProvider.addResponseInterceptor(function (data, operation, what, url, response) {
        if (data) {
            console.log(url, data, operation);
        }
        return data;
    });
    // Every time when error is received from the server API_ENDPOINT, first it is processed here.
    // IDM_ENDPOINT errors are processed in the functions only (login, refresh-tokens, get-users x 2).
    RestangularProvider.addErrorInterceptor(function (response, subject, responseHandler) { return __awaiter(_this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            console.log('ErrorInterceptor', response);
            return [2 /*return*/];
        });
    }); });
}
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_faq_faq__["a" /* FaqPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_my_account_my_account__["a" /* MyAccountPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_suggest_suggest__["a" /* SuggestPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_forgotpass_forgotpass__["a" /* ForgotpassPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_company_company__["a" /* CompanyPage */],
                __WEBPACK_IMPORTED_MODULE_22__components_footer_footer__["a" /* FooterComponent */],
                __WEBPACK_IMPORTED_MODULE_23__components_waze_modal_waze_modal__["a" /* WazeModalComponent */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/about/about.module#AboutPageModule', name: 'AboutPage', segment: 'about', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/faq/faq.module#FaqPageModule', name: 'FaqPage', segment: 'faq', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/company/company.module#CompanyPageModule', name: 'CompanyPage', segment: 'company', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/forgotpass/forgotpass.module#ForgotpassPageModule', name: 'ForgotpassPage', segment: 'forgotpass', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/my-account/my-account.module#MyAccountPageModule', name: 'MyAccountPage', segment: 'my-account', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/suggest/suggest.module#SuggestPageModule', name: 'SuggestPage', segment: 'suggest', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_25_ngx_restangular__["RestangularModule"].forRoot([], RestangularConfigFactory),
                __WEBPACK_IMPORTED_MODULE_21__angular_http__["b" /* HttpModule */],
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_faq_faq__["a" /* FaqPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_my_account_my_account__["a" /* MyAccountPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_suggest_suggest__["a" /* SuggestPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_forgotpass_forgotpass__["a" /* ForgotpassPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_company_company__["a" /* CompanyPage */],
                __WEBPACK_IMPORTED_MODULE_22__components_footer_footer__["a" /* FooterComponent */],
                __WEBPACK_IMPORTED_MODULE_23__components_waze_modal_waze_modal__["a" /* WazeModalComponent */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_social_sharing__["a" /* SocialSharing */],
                __WEBPACK_IMPORTED_MODULE_16__services_loginService__["a" /* loginService */],
                __WEBPACK_IMPORTED_MODULE_20__services_toastService__["a" /* toastService */],
                __WEBPACK_IMPORTED_MODULE_17__services_config__["a" /* Config */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_launch_navigator__["a" /* LaunchNavigator */],
                __WEBPACK_IMPORTED_MODULE_24__services_sent_to_server_service__["a" /* sent_to_server_service */],
                __WEBPACK_IMPORTED_MODULE_26__ionic_native_in_app_browser__["a" /* InAppBrowser */],
                __WEBPACK_IMPORTED_MODULE_27__ionic_native_keyboard__["a" /* Keyboard */],
                __WEBPACK_IMPORTED_MODULE_29__ionic_native_facebook__["a" /* Facebook */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicErrorHandler */] },
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return loginService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var loginService = (function () {
    function loginService(http, Settings) {
        this.http = http;
        this.Settings = Settings;
        this.ServerUrl = Settings.ServerUrl;
        this.CustomerId = Settings.CustomerId;
    }
    ;
    loginService.prototype.getUserDetails = function (url, info) {
        //console.log("Info : " , info)
        var body = new FormData();
        body.append('customer_id', this.CustomerId);
        body.append('mail', info["mail"]);
        body.append('password', info["password"]);
        body.append('push', this.Settings.PushId);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("getUserDetails : ", data); }).toPromise();
    };
    loginService.prototype.facebookLogin = function (url, email) {
        //console.log("Info : " , info)
        var body = new FormData();
        body.append('customer_id', this.CustomerId);
        body.append('mail', email);
        body.append('push', this.Settings.PushId);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("getUserDetails : ", data); }).toPromise();
    };
    loginService.prototype.logOutUser = function () {
        window.localStorage.identify = '';
        this.Settings.UserId = 0;
    };
    loginService.prototype.ForgotPass = function (url, info) {
        var body = new FormData();
        body.append('customer_id', this.CustomerId);
        body.append('info', JSON.stringify(info));
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("ForgotPass : ", data); }).toPromise();
    };
    loginService.prototype.RegisterUser = function (url, info) {
        var body = new FormData();
        body.append('customer_id', this.CustomerId);
        body.append('info', JSON.stringify(info));
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("getUserDetails : ", data); }).toPromise();
    };
    loginService.prototype.sendRecommend = function (url, info) {
        var body = new FormData();
        body.append('customer_id', this.CustomerId);
        body.append('info', JSON.stringify(info));
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("getUserDetails : ", data); }).toPromise();
    };
    loginService.prototype.getRecommendById = function (url) {
        var body = new FormData();
        body.append('customer_id', this.CustomerId);
        body.append('uid', window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("getUserDetails : ", data); }).toPromise();
    };
    loginService.prototype.getUserById = function (url) {
        var body = new FormData();
        body.append('customer_id', this.CustomerId);
        body.append('uid', window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("getUserDetails : ", data); }).toPromise();
    };
    loginService.prototype.SaveDetails = function (url, Details) {
        var body = new FormData();
        body.append('customer_id', this.CustomerId);
        body.append('uid', window.localStorage.id);
        body.append('selected', JSON.stringify(Details));
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("getUserDetails : ", data); }).toPromise();
    };
    loginService.prototype.requestWithdraw = function (url) {
        var body = new FormData();
        body.append('customer_id', this.CustomerId);
        body.append('uid', window.localStorage.id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("getUserDetails : ", data); }).toPromise();
    };
    loginService.prototype.sendTestData = function (url, data) {
        var body = new FormData();
        body.append('customer_id', this.CustomerId);
        body.append('data', data);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { console.log("ForgotPass : ", data); }).toPromise();
    };
    loginService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_3__config__["a" /* Config */]])
    ], loginService);
    return loginService;
}());

;
//# sourceMappingURL=loginService.js.map

/***/ }),

/***/ 57:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__about_about__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__faq_faq__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__my_account_my_account__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__suggest_suggest__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__profile_profile__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_launch_navigator__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_social_sharing__ = __webpack_require__(183);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_sent_to_server_service__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_config__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_platform_browser__ = __webpack_require__(23);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var HomePage = (function () {
    function HomePage(navCtrl, launchNavigator, socialSharing, sendtoserver, zone, Settings, domSanitizer) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.launchNavigator = launchNavigator;
        this.socialSharing = socialSharing;
        this.sendtoserver = sendtoserver;
        this.zone = zone;
        this.Settings = Settings;
        this.domSanitizer = domSanitizer;
        this.CustumerServerData = [];
        this.CustomerId = this.Settings.CustomerId;
        this.host = this.Settings.host;
        this.sendtoserver._JsonInfo.subscribe(function (val) {
            _this.zone.run(function () {
                _this.CustumerServerData = val;
                console.log("CustumerServerData", _this.CustumerServerData);
                if (_this.CustumerServerData) {
                    if (_this.CustumerServerData.custumer_data) {
                        if (_this.CustumerServerData.app_settings.length > 0) {
                            _this.AppTitle = _this.CustumerServerData.app_settings[0].title;
                            _this.AppWebSite = _this.CustumerServerData.app_settings[0].website;
                            _this.AppDesc = _this.CustumerServerData.app_settings[0].description;
                        }
                        if (_this.CustumerServerData.custumer_data.length > 0) {
                            _this.CustumerName = _this.CustumerServerData.custumer_data[0].name;
                            _this.imageLogo = _this.host + _this.CustumerServerData.custumer_data[0].image;
                            _this.website = _this.CustumerServerData.custumer_data[0].website;
                            _this.description = _this.CustumerServerData.custumer_data[0].description;
                            _this.mainpagevideo = _this.CustumerServerData.custumer_data[0].home_video + '?rel=0';
                            _this.mainpagevideo = _this.domSanitizer.bypassSecurityTrustResourceUrl(_this.mainpagevideo);
                        }
                    }
                }
            });
        });
    }
    HomePage.prototype.goToAbout = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__about_about__["a" /* AboutPage */]);
    };
    HomePage.prototype.goToFAQ = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__faq_faq__["a" /* FaqPage */]);
    };
    HomePage.prototype.goToMyAccount = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__my_account_my_account__["a" /* MyAccountPage */]);
    };
    HomePage.prototype.goToSuggest = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__suggest_suggest__["a" /* SuggestPage */]);
    };
    HomePage.prototype.goToProfile = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__profile_profile__["a" /* ProfilePage */]);
    };
    HomePage.prototype.shareViaWhatsApp = function () {
        //this.socialSharing.shareViaWhatsApp(this.Message , this.Image , this.AppUrl);
        this.socialSharing.shareViaSMS(window.localStorage.name + " " + "שיתף איתך לינק" + " " + this.description + " " + this.AppWebSite, null);
        console.log("sssss");
        //this.socialSharing.shareViaFacebook(this.Message, this.Image, this.AppUrl)
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\baloon\src\pages\home\home.html"*/'<ion-header>\n\n    <ion-navbar no-border-top class="ToolBarClass">\n\n        <button ion-button menuToggle end="">\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>\n\n            <div style="width: 100%">\n\n                <img [src]="imageLogo" *ngIf="imageLogo" class="headLogo"/>\n\n            </div>\n\n        </ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n    <div class="Video">\n\n        <iframe width="100%" height="230" [src]="mainpagevideo" *ngIf="mainpagevideo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>\n\n    </div>\n\n    <div class="verticalLine"></div>\n\n    <div class="mainIcons">\n\n        <div class="mainRow">\n\n            <div class="mainRowLeft" align="center" (click)="goToProfile()" >\n\n                <div class="mainRowDiv">\n\n                    <img src="images/mi2.png" style="width: 100%"/>\n\n                </div>\n\n            </div>\n\n            <div class="mainRowRight" (click)="goToSuggest()">\n\n                <div class="mainRowDiv">\n\n                    <img src="images/mi1.png" style="width: 100%; margin-left: 30px;"/>\n\n                </div>\n\n            </div>\n\n        </div>\n\n        <div class="mainRow">\n\n            <div class="mainRowLeft" align="center" (click)="goToMyAccount()">\n\n                <div class="mainRowDiv">\n\n                    <img src="images/mi3.png" style="width: 100%"/>\n\n                </div>\n\n            </div>\n\n            <div class="mainRowRight" (click)="goToFAQ()">\n\n                <div class="mainRowDiv">\n\n                    <img src="images/mi4.png" style="width: 100%; margin-left: 30px;"/>\n\n                </div>\n\n            </div>\n\n        </div>\n\n        <div class="mainRowNoBorder">\n\n            <div class="mainRowLeft" align="center" (click)="goToAbout()">\n\n                <div class="mainRowDiv">\n\n                    <img src="images/mi5.png" style="width: 100%"/>\n\n                </div>\n\n            </div>\n\n            <div class="mainRowRight" (click)="shareViaWhatsApp()">\n\n                <div class="mainRowDiv">\n\n                    <img src="images/mi6.png" style="width: 100%; margin-left: 30px;"/>\n\n                </div>\n\n            </div>\n\n        </div>\n\n    </div>\n\n</ion-content>\n\n<ion-footer style="padding: 0px; margin: 0px; background-color: #144e6e">\n\n    <footer></footer>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Desktop\github\baloon\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_launch_navigator__["a" /* LaunchNavigator */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_social_sharing__["a" /* SocialSharing */], __WEBPACK_IMPORTED_MODULE_9__services_sent_to_server_service__["a" /* sent_to_server_service */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_10__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_11__angular_platform_browser__["c" /* DomSanitizer */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 790:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(418);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(419);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_loginService__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_sent_to_server_service__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_config__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_register_register__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_in_app_browser__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_about_about__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_faq_faq__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_my_account_my_account__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_suggest_suggest__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_profile_profile__ = __webpack_require__(86);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
















var MyApp = (function () {
    function MyApp(platform, loginService, statusBar, Settings, splashScreen, sendtoserver, iab) {
        this.platform = platform;
        this.loginService = loginService;
        this.statusBar = statusBar;
        this.Settings = Settings;
        this.splashScreen = splashScreen;
        this.sendtoserver = sendtoserver;
        this.iab = iab;
        this.rootPage = ""; //LoginPage;
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'ראשי', component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */], icon: 'ios-home-outline' },
            { title: 'המלץ לחבר', component: __WEBPACK_IMPORTED_MODULE_14__pages_suggest_suggest__["a" /* SuggestPage */], icon: 'ios-people-outline' },
            { title: 'פרטים אישיים', component: __WEBPACK_IMPORTED_MODULE_15__pages_profile_profile__["a" /* ProfilePage */], icon: 'ios-contact-outline' },
            { title: 'שאלות ותשובות', component: __WEBPACK_IMPORTED_MODULE_12__pages_faq_faq__["a" /* FaqPage */], icon: 'ios-help-outline' },
            { title: 'החשבון שלי', component: __WEBPACK_IMPORTED_MODULE_13__pages_my_account_my_account__["a" /* MyAccountPage */], icon: 'ios-card-outline' },
            { title: 'מי אנחנו', component: __WEBPACK_IMPORTED_MODULE_11__pages_about_about__["a" /* AboutPage */], icon: 'ios-information-circle-outline' },
            { title: 'תנאי שימוש', component: 'TermsPage', icon: 'ios-checkbox-outline' },
            { title: 'התנתק', component: 'LogOut', icon: 'ios-close-circle-outline' }
        ];
        console.log("CustomerId:", this.Settings.CustomerId);
        console.log("Local : ", window.localStorage.id, window.localStorage.sid);
        if (window.localStorage.id == undefined || window.localStorage.id == '')
            this.rootPage = __WEBPACK_IMPORTED_MODULE_9__pages_register_register__["a" /* RegisterPage */]; //RegisterPage;//
        else
            this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */]; // ChatPage; //
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            _this.getCustumerServerData();
            if (_this.platform.is('ios')
                || _this.platform.is('android')
                || _this.platform.is('windows')) {
                //enableProdMode();
            }
        });
    };
    MyApp.prototype.getCustumerServerData = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.sendtoserver.getCustumerServerData('getCustumerServerData')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.component == 'LogOut') {
            window.localStorage.id = '';
            this.Settings.User = '';
            this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */]);
            console.log("Out");
        }
        else if (page.component == 'TermsPage') {
            this.browser = this.iab.create(this.Settings.TermsURL, '_system', 'location=no,toolbar=yes');
        }
        else
            this.nav.push(page.component);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"C:\Users\USER\Desktop\github\baloon\src\app\app.html"*/'<!--<ion-menu [content]="content" side="right">-->\n\n  <!--<ion-header>-->\n\n    <!--<ion-toolbar>-->\n\n      <!--<ion-title>Menu</ion-title>-->\n\n    <!--</ion-toolbar>-->\n\n  <!--</ion-header>-->\n\n\n\n  <!--<ion-content>-->\n\n    <!--<ion-list>-->\n\n      <!--<button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">-->\n\n        <!--{{p.title}}-->\n\n      <!--</button>-->\n\n    <!--</ion-list>-->\n\n  <!--</ion-content>-->\n\n\n\n<!--</ion-menu>-->\n\n\n\n<!--&lt;!&ndash; Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus &ndash;&gt;-->\n\n<!--<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>-->\n\n\n\n\n\n<ion-menu [content]="content" side="right">\n\n    <ion-header>\n\n        <ion-toolbar  style="background-color: #022850 !important;">\n\n            <ion-title style="text-align: right;">תפריט</ion-title>\n\n        </ion-toolbar>\n\n    </ion-header>\n\n\n\n    <ion-content>\n\n        <ion-list>\n\n            <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)" style="text-align: right; direction: rtl">\n\n                <ion-icon class="sideMenuIcon" name="{{p.icon}}"></ion-icon>\n\n                <span class="sideMenuText">{{p.title}}</span>\n\n            </button>\n\n        </ion-list>\n\n        <div style="width:50%; margin:auto;" align="center">\n\n            <img src="images/newlogo.png" style="width:100%;">\n\n        </div>\n\n    </ion-content>\n\n\n\n</ion-menu>\n\n\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false" ></ion-nav>'/*ion-inline-end:"C:\Users\USER\Desktop\github\baloon\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */], __WEBPACK_IMPORTED_MODULE_6__services_loginService__["a" /* loginService */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_8__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_7__services_sent_to_server_service__["a" /* sent_to_server_service */], __WEBPACK_IMPORTED_MODULE_10__ionic_native_in_app_browser__["a" /* InAppBrowser */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 791:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ListPage = (function () {
    function ListPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        // Let's populate this page with some filler content for funzies
        this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
            'american-football', 'boat', 'bluetooth', 'build'];
        this.items = [];
        for (var i = 1; i < 11; i++) {
            this.items.push({
                title: 'Item ' + i,
                note: 'This is item #' + i,
                icon: this.icons[Math.floor(Math.random() * this.icons.length)]
            });
        }
    }
    ListPage_1 = ListPage;
    ListPage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(ListPage_1, {
            item: item
        });
    };
    ListPage = ListPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-list',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\baloon\src\pages\list\list.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>List</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-list>\n\n    <button ion-item *ngFor="let item of items" (click)="itemTapped($event, item)">\n\n      <ion-icon [name]="item.icon" item-start></ion-icon>\n\n      {{item.title}}\n\n      <div class="item-note" item-end>\n\n        {{item.note}}\n\n      </div>\n\n    </button>\n\n  </ion-list>\n\n  <div *ngIf="selectedItem" padding>\n\n    You navigated here from <b>{{selectedItem.title}}</b>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Desktop\github\baloon\src\pages\list\list.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
    ], ListPage);
    return ListPage;
    var ListPage_1;
}());

//# sourceMappingURL=list.js.map

/***/ }),

/***/ 792:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_social_sharing__ = __webpack_require__(183);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_launch_navigator__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__waze_modal_waze_modal__ = __webpack_require__(421);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_sent_to_server_service__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_config__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_company_company__ = __webpack_require__(420);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the FooterComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var FooterComponent = (function () {
    function FooterComponent(navCtrl, socialSharing, launchNavigator, modalCtrl, sendtoserver, zone, Settings) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.socialSharing = socialSharing;
        this.launchNavigator = launchNavigator;
        this.modalCtrl = modalCtrl;
        this.sendtoserver = sendtoserver;
        this.zone = zone;
        this.Settings = Settings;
        this.CustumerServerData = [];
        this.customers_branches = [];
        this.CustomerId = this.Settings.CustomerId;
        this.host = this.Settings.host;
        this.sendtoserver._JsonInfo.subscribe(function (val) {
            _this.zone.run(function () {
                _this.CustumerServerData = val;
                console.log("CustumerServerData", _this.CustumerServerData);
                if (_this.CustumerServerData) {
                    if (_this.CustumerServerData.custumer_data) {
                        if (_this.CustumerServerData.app_settings.length > 0) {
                            _this.AppTitle = _this.CustumerServerData.app_settings[0].title;
                            _this.AppWebSite = _this.CustumerServerData.app_settings[0].website;
                            _this.AppDesc = _this.CustumerServerData.app_settings[0].description;
                        }
                        if (_this.CustumerServerData.custumer_data.length > 0) {
                            _this.MailSubject = _this.CustumerServerData.custumer_data[0].name;
                            _this.Image = _this.host + _this.CustumerServerData.custumer_data[0].image;
                            _this.Message = _this.CustumerServerData.custumer_data[0].description;
                            _this.AppUrl = _this.CustumerServerData.custumer_data[0].website;
                            _this.PhoneNumber = _this.CustumerServerData.custumer_data[0].phone;
                            _this.Email = _this.CustumerServerData.custumer_data[0].email;
                        }
                    }
                }
            });
        });
    }
    FooterComponent.prototype.shareViaPhone = function () {
        window.location.href = "tel:" + this.PhoneNumber;
    };
    FooterComponent.prototype.shareViaMail = function () {
        this.socialSharing.shareViaEmail(this.Message, "הערה, תלונה או בעיה", [this.Email]).then(function () {
            // Success!
        }).catch(function () {
            // Error!
        });
    };
    FooterComponent.prototype.openCompanyPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__pages_company_company__["a" /* CompanyPage */]);
    };
    FooterComponent.prototype.share = function () {
        this.socialSharing.share(this.Message, this.MailSubject, this.Image, this.AppWebSite);
    };
    FooterComponent.prototype.shareViaWaze = function () {
        var _this = this;
        var noteModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__waze_modal_waze_modal__["a" /* WazeModalComponent */]);
        noteModal.present();
        noteModal.onDidDismiss(function (data) {
            console.log(data);
            _this.launchNavigator.navigate([data.location_lat, data.location_lng]);
        });
        // this.launchNavigator.navigate([32.097143, 34.821450]);
        //window.location.href = "waze://?ll=32.123286,34.808958"
        //<a href='waze://?ll=31.99090,34.77444'>Let's go to the mall</a>
    };
    FooterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'footer',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\baloon\src\components\footer\footer.html"*/'<!-- Generated template for the FooterComponent component -->\n\n<table>\n\n    <tr>\n\n        <td class="mainTd" (click)="openCompanyPage()">\n\n            <img src="images/company.png" style="width: 100%;"/>\n\n        </td>\n\n        <td class="mainTd" (click)="shareViaWaze()">\n\n            <img src="images/fi2.png" style="width: 100%;"/>\n\n        </td>\n\n        <td class="mainTd" (click)="shareViaMail()">\n\n            <img src="images/fi3.png" style="width: 100%;"/>\n\n        </td>\n\n        <td (click)="shareViaPhone()">\n\n            <img src="images/fi4.png" style="width: 100%;"/>\n\n        </td>\n\n    </tr>\n\n</table>'/*ion-inline-end:"C:\Users\USER\Desktop\github\baloon\src\components\footer\footer.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1__ionic_native_social_sharing__["a" /* SocialSharing */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_launch_navigator__["a" /* LaunchNavigator */], __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["h" /* ModalController */], __WEBPACK_IMPORTED_MODULE_5__services_sent_to_server_service__["a" /* sent_to_server_service */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_6__services_config__["a" /* Config */]])
    ], FooterComponent);
    return FooterComponent;
}());

//# sourceMappingURL=footer.js.map

/***/ }),

/***/ 84:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return toastService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var toastService = (function () {
    function toastService(toastCtrl) {
        this.toastCtrl = toastCtrl;
        console.log("shay");
    }
    toastService.prototype.presentToast = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'bottom',
            cssClass: 'ToastClass'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        toast.present();
    };
    toastService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["m" /* ToastController */]])
    ], toastService);
    return toastService;
}());

//# sourceMappingURL=toastService.js.map

/***/ }),

/***/ 85:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_loginService__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_config__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__register_register__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_toastService__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_sent_to_server_service__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_in_app_browser__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__forgotpass_forgotpass__ = __webpack_require__(377);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_facebook__ = __webpack_require__(378);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = (function () {
    function LoginPage(navCtrl, navParams, Login, Settings, Toast, toastCtrl, sendtoserver, zone, iab, fb) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Login = Login;
        this.Settings = Settings;
        this.Toast = Toast;
        this.toastCtrl = toastCtrl;
        this.sendtoserver = sendtoserver;
        this.zone = zone;
        this.iab = iab;
        this.fb = fb;
        this.info = { mail: "", password: '', agreeTerms: false };
        this.user = '';
        this.CustumerServerData = [];
        this.isLoggedIn = false;
        this.CustomerId = this.Settings.CustomerId;
        this.host = this.Settings.host;
        this.sendtoserver._JsonInfo.subscribe(function (val) {
            _this.zone.run(function () {
                _this.CustumerServerData = val;
                console.log("CustumerServerData", _this.CustumerServerData);
                if (_this.CustumerServerData) {
                    if (_this.CustumerServerData.custumer_data) {
                        if (_this.CustumerServerData.custumer_data.length > 0) {
                            _this.imageLogo = _this.host + _this.CustumerServerData.custumer_data[0].image;
                        }
                    }
                }
            });
        });
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.toggleTerms = function (type) {
        this.info.agreeTerms = type;
    };
    LoginPage.prototype.openTerms = function () {
        this.browser = this.iab.create(this.Settings.TermsURL, '_system', 'location=no,toolbar=yes');
    };
    LoginPage.prototype.openForgotPassPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__forgotpass_forgotpass__["a" /* ForgotpassPage */]);
    };
    LoginPage.prototype.checkLogin = function () {
        var _this = this;
        this.emailregex = /\S+@\S+\.\S+/;
        if (this.info['mail'].length == 0)
            this.Toast.presentToast('הכנס כתובת מייל');
        else if (!this.emailregex.test(this.info['mail'])) {
            this.info['mail'] = '';
            this.Toast.presentToast('מייל לא תקין');
        }
        else if (this.info['password'].length == 0)
            this.Toast.presentToast('הכנס סיסמה');
        else if (!this.info.agreeTerms) {
            this.Toast.presentToast('יש לאשר תנאי שימוש');
        }
        else {
            //console.log(this.info);
            this.Login.getUserDetails('getUserDetails', this.info).then(function (data) {
                if (data[0]) {
                    console.log("UserDetails : ");
                    _this.user = data[0];
                    _this.setUserSettings(data);
                }
                else {
                    console.log("UserDetails : ", data[0]);
                    _this.Toast.presentToast('פרטים לא נכונים אנא נסה שנית');
                    _this.info['password'] = '';
                }
            });
        }
    };
    LoginPage.prototype.faceBookLogin = function () {
        var _this = this;
        console.log("Facebook connect");
        this.fb.login(['public_profile', 'email'])
            .then(function (res) {
            if (res.status === "connected") {
                //this.isLoggedIn = true;
                _this.getUserDetail(res.authResponse.userID);
            }
            else {
                //this.isLoggedIn = false;
                //alert(JSON.stringify(res));
            }
        })
            .catch(function (e) {
            return console.log(e);
        });
    };
    LoginPage.prototype.getUserDetail = function (userid) {
        var _this = this;
        this.fb.api("/" + userid + "/?fields=id,email,name,gender", ["public_profile"])
            .then(function (res) {
            console.log(res);
            //alert(JSON.stringify(res));
            //alert (res['email']);
            _this.Login.facebookLogin('facebookLogin', res['email']).then(function (data) {
                if (data[0]) {
                    console.log("UserDetails : ");
                    _this.user = data[0];
                    _this.setUserSettings(data);
                }
                else {
                    //console.log("UserDetails : " , data[0]);
                    _this.Toast.presentToast("משתמש לא נמצא יש תחילה להרשם");
                    //this.info['password'] = '';
                }
            });
        })
            .catch(function (e) {
            //console.log(e);
        });
    };
    LoginPage.prototype.setUserSettings = function (data) {
        console.log("MMM : ", this.user);
        window.localStorage.id = this.user["id"];
        window.localStorage.name = this.user["name"];
        window.localStorage.info = this.user["info"];
        window.localStorage.image = this.user["image"];
        window.localStorage.type = this.user["type_id"];
        this.Settings.UserConnected = this.user;
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
    };
    LoginPage.prototype.GoToRegister = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__register_register__["a" /* RegisterPage */]);
    };
    LoginPage.prototype.presentToast = function (text) {
        console.log("f3");
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'bottom',
            cssClass: "ToastClass"
        });
        toast.present();
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\baloon\src\pages\login\login.html"*/'\n\n<ion-content padding class="loginMain">\n\n  <div >\n\n      <div style="width: 80%; margin:auto; margin-top: 40px;">\n\n          <img [src]="imageLogo" style="width: 100%" />\n\n      </div>\n\n\n\n    <div class="InputsText" align="center">\n\n      <div class="InputText">\n\n        <input type="email" placeholder="הכנס מייל תקין" [(ngModel)]="info.mail" />\n\n      </div>\n\n      <div class="InputText mt10">\n\n        <input type="password" placeholder="הכנס סיסמה" [(ngModel)]="info.password" />\n\n      </div>\n\n\n\n\n\n        <div style="font-size:15px; padding-bottom:10px; padding-top:5px;">\n\n            <ion-icon name="ios-checkbox-outline" *ngIf="!info.agreeTerms"  style="font-size:30px;" (click)="toggleTerms(true)"></ion-icon>\n\n            <ion-icon name="md-checkbox-outline" *ngIf="info.agreeTerms" style="font-size:30px;" (click)="toggleTerms(false)"></ion-icon>\n\n\n\n\n\n            <span>יש להסכים לתנאי השימוש</span>\n\n            <span (click)="openTerms()"><a href="#">תנאי שימוש </a></span>\n\n        </div>\n\n\n\n\n\n        <button class="loginButton"  ion-button block (click)="checkLogin()">התחל</button>\n\n\n\n        <button class="loginButton" (click)="faceBookLogin()" ion-button block icon-end>\n\n            התחברות עם פייסבוק\n\n            <ion-icon name="logo-facebook"></ion-icon>\n\n        </button>\n\n\n\n        <div>\n\n\n\n        </div>\n\n\n\n\n\n        <a class="register" (click)="GoToRegister()">הרשמ/י למערכת</a>\n\n        <div style="margin-top:5px;">\n\n            <a class="register" (click)="openForgotPassPage()">שכחתי סיסמה</a>\n\n        </div>\n\n    </div>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\USER\Desktop\github\baloon\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__services_loginService__["a" /* loginService */], __WEBPACK_IMPORTED_MODULE_3__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_6__services_toastService__["a" /* toastService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ToastController */], __WEBPACK_IMPORTED_MODULE_7__services_sent_to_server_service__["a" /* sent_to_server_service */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_8__ionic_native_in_app_browser__["a" /* InAppBrowser */], __WEBPACK_IMPORTED_MODULE_10__ionic_native_facebook__["a" /* Facebook */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 86:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_loginService__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_config__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_sent_to_server_service__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__ = __webpack_require__(23);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProfilePage = (function () {
    function ProfilePage(navCtrl, Login, navParams, sendtoserver, zone, Settings, domSanitizer) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.Login = Login;
        this.navParams = navParams;
        this.sendtoserver = sendtoserver;
        this.zone = zone;
        this.Settings = Settings;
        this.domSanitizer = domSanitizer;
        this.Details = '';
        this.CustumerServerData = [];
        this.CustomerId = this.Settings.CustomerId;
        this.host = this.Settings.host;
        this.sendtoserver._JsonInfo.subscribe(function (val) {
            _this.zone.run(function () {
                _this.CustumerServerData = val;
                console.log("CustumerServerData", _this.CustumerServerData);
                if (_this.CustumerServerData) {
                    if (_this.CustumerServerData.custumer_data) {
                        if (_this.CustumerServerData.custumer_data.length > 0) {
                            _this.imageLogo = _this.host + _this.CustumerServerData.custumer_data[0].image;
                            _this.personal_details_video = _this.CustumerServerData.custumer_data[0].personal_details_video + '?rel=0';
                            ;
                            _this.personal_details_video = _this.domSanitizer.bypassSecurityTrustResourceUrl(_this.personal_details_video);
                        }
                    }
                }
            });
        });
        this.Login.getUserById('getUserById').then(function (data) {
            _this.Details = data[0];
            console.log("getUserById : ", _this.Details);
        });
    }
    ProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilePage');
    };
    ProfilePage.prototype.saveDetails = function () {
        var _this = this;
        this.Login.SaveDetails('SaveProfile', this.Details).then(function (data) {
            console.log("SaveProfile11 : ", data);
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
        });
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-profile',template:/*ion-inline-start:"C:\Users\USER\Desktop\github\baloon\src\pages\profile\profile.html"*/'<ion-header>\n\n    <ion-navbar no-border-top class="ToolBarClass">\n\n        <button ion-button menuToggle end="">\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>\n\n            <div style="width: 100%">\n\n                <img [src]="imageLogo" class="headLogo1"/>\n\n            </div>\n\n        </ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n    <div class="Video">\n\n        <iframe width="100%" height="230" [src]="personal_details_video" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>\n\n    </div>\n\n    <div align="center" *ngIf="Details != \'\'">\n\n\n\n        <div class="friendName">\n\n            <input type="text" [(ngModel)]="Details.name" class="friendNameText" placeholder="שם מלא">\n\n        </div>\n\n        <!--\n\n        <div class="friendName">\n\n            <input type="text" class="friendNameText" [(ngModel)]="Details.mail" placeholder="אימייל">\n\n        </div>\n\n        -->\n\n        <div class="friendName">\n\n            <input type="tel" class="friendNameText" [(ngModel)]="Details.phone" placeholder="מספר טלפון">\n\n        </div>\n\n\n\n        <div class="friendName">\n\n            <input type="tel" class="friendNameText" [(ngModel)]="Details.password" placeholder="הסיסמה שלך באפליקצייה">\n\n        </div>\n\n\n\n        <div class="friendName">\n\n            <input type="text" class="friendNameText" [(ngModel)]="Details.accout_name" placeholder="שם בעל חשבון הבנק">\n\n        </div>\n\n\n\n        <div class="friendName">\n\n            <input type="text" class="friendNameText" [(ngModel)]="Details.bank_number" placeholder="שם הבנק">\n\n        </div>\n\n\n\n        <div class="friendName">\n\n            <input type="tel" class="friendNameText" [(ngModel)]="Details.snif_number" placeholder="מספר סניף">\n\n        </div>\n\n\n\n        <div class="friendName">\n\n            <input type="tel" class="friendNameText" [(ngModel)]="Details.bank_acoount_number" placeholder="מספר חשבון">\n\n        </div>\n\n\n\n        <button ion-button class="suggestSendButton" (click)="saveDetails()">עדכן פרטים</button>\n\n    </div>\n\n</ion-content>\n\n\n\n\n\n<ion-footer style="padding: 0px; margin: 0px; background-color: #144e6e">\n\n\n\n</ion-footer>'/*ion-inline-end:"C:\Users\USER\Desktop\github\baloon\src\pages\profile\profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__services_loginService__["a" /* loginService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_5__services_sent_to_server_service__["a" /* sent_to_server_service */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_4__services_config__["a" /* Config */], __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["c" /* DomSanitizer */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ })

},[422]);
//# sourceMappingURL=main.js.map